"""PBX URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
# from pbx.views import CreateView
# from pbx.serializers import SIPSerializer

# sip_res = SIPSerializer()
#
# urlpatterns = [
#     url(r'^admin/', admin.site.urls),
#     url(r'^sip/', include(sip_res.urls),name="SIP"),
#     # url(r'^sip/$', CreateView.), name="SIP"),
# ]


from django.conf.urls import url, include

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('pbx.urls')) # Add this line
]