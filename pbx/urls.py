from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.authtoken.views import obtain_auth_token
from .views import CreateView, UpdateView, UserView, sip_list

urlpatterns = {
    url(r'^auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^c/$', CreateView.as_view(), name="create"),
    url(r'^u/(?P<pk>[0-9]+)/$', UpdateView.as_view(), name="details"),
    url(r'^users/$', UserView.as_view(), name="users"),
    # url(r'users/(?P<pk>[0-9]+)/$', UserDetailsView.as_view(), name="user_details"),
    url(r'^get-token/', obtain_auth_token), # Add this line
    url(r'sip/', sip_list),
}

urlpatterns = format_suffix_patterns(urlpatterns)



# *********************** how to use routing *******************************
#
# from rest_framework import routers
# from tutorial.quickstart import views
#
# router = routers.DefaultRouter()
# router.register(r'users', views.UserViewSet)
# router.register(r'groups', views.GroupViewSet)
#
# # Wire up our API using automatic URL routing.
# # Additionally, we include login URLs for the browsable API.
# urlpatterns = [
#     path('', include(router.urls)),
#     path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
# ]
#
# *********************** how to use routing *******************************END
