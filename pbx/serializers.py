from models import *
from rest_framework import serializers
from models import sip_buddies

from tastypie.resources import ModelResource


# class SIPSerializer(serializers.ModelSerializer):
# class SIPSerializer(ModelResource):
#
#     class Meta:
#         queryset = sip_buddies.objects.all()
#         resource_name = 'sip'
#         fields = ('id', 'name', 'host', 'nat', 'type', 'ipaddr', 'port', 'fullcontact', 'lastms')



class SIPSerializer(serializers.ModelSerializer):
    """Serializer to map the Model instance into JSON format."""

    # owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = sip_buddies
        fields = ('id', 'name', 'host', 'nat', 'ipaddr', 'secret')


class USERSerializer(serializers.ModelSerializer):
    """Serializer to map the Model instance into JSON format."""

    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = User
        fields = '__all__'