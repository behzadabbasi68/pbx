# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import unicode_literals
from models import *
from rest_framework import generics
from rest_framework.decorators import api_view, authentication_classes
from rest_framework.response import Response
from rest_framework import permissions
from serializers import SIPSerializer, USERSerializer
from models import sip_buddies
from rest_framework.authentication import SessionAuthentication, BasicAuthentication


class CreateView(generics.ListCreateAPIView):
    queryset = sip_buddies.objects.filter(name='203')
    serializer_class = SIPSerializer

    # permission_classes = (permissions.IsAuthenticated,)
    def perform_create(self, serializer):
        print('request is :' + str(self.request.method))
        # serializer.save(owner=self.request.user)
        serializer.save()

    def get_queryset(self):
        """
        This view should return a list of all the purchases for
        the user as determined by the username portion of the URL.
        """
        return super(CreateView, self).get_queryset()


class UpdateView(generics.RetrieveUpdateDestroyAPIView):
    # authentication_classes = (SessionAuthentication, BasicAuthentication)
    queryset = sip_buddies.objects.all()
    serializer_class = SIPSerializer
    pk = None

    # permission_classes = (permissions.IsAuthenticated,)


    def perform_update(self, serializer):
        serializer.save(owner=self.request.user)


class UserView(generics.RetrieveUpdateAPIView):
    queryset = User.objects.all()
    serializer_class = USERSerializer

    def get_object(self, *args, **kwargs):
        return self.request.user

    def perform_update(self, serializer):
        super(UserView, self).perform_update(serializer)


# *************************** Without View Form ***************************

@api_view(['GET', 'POST'])
# @authentication_classes((SessionAuthentication, BasicAuthentication))
def sip_list(request):
    if request.method == 'GET':
        queryset = sip_buddies.objects.all()
        serializer = SIPSerializer()
        print(serializer.data)
        return Response(serializer.data)
