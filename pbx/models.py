# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from django.dispatch import receiver


# Create your models here.
class sip_buddies(models.Model):
    #Remove id from model
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=80, unique=True)
    host = models.CharField(max_length=30)
    peerType = (
        ('user', 'USER'),
        ('peer', 'PEER'),
        ('friend', 'FRIEND'),
    )
    type = models.CharField(max_length=10, choices=peerType)
    ipaddr = models.CharField(max_length=30, blank=True, null=True, default='')
    port = models.PositiveIntegerField(blank=True, null=True)
    regseconds = models.PositiveIntegerField(blank=True, null=True)
    defaultuser = models.CharField(max_length=10, blank=True, null=True, default='')
    fullcontact = models.CharField(max_length=35, blank=True, null=True, default='')
    regserver = models.CharField(max_length=20, blank=True, null=True, default='')
    useragent = models.CharField(max_length=20, blank=True, null=True, default='')
    lastms = models.PositiveIntegerField(blank=True, null=True)
    context = models.CharField(max_length=40)
    permit = models.CharField(max_length=40, blank=True, null=True, default='')
    deny = models.CharField(max_length=40, blank=True, null=True, default='')
    secret = models.CharField(max_length=40)
    md5secret = models.CharField(max_length=40, blank=True, null=True, default='')
    remotesecret = models.CharField(max_length=40, blank=True, null=True, default='')
    transportType = (
        ('udp', 'UDP'),
        ('tcp', 'TCP'),
        ('udp,tcp', 'UT'),
        ('tcp,udp', 'TU'),
    )
    transport = models.CharField(max_length=10, choices=transportType, blank=True, null=True, default='')
    dtmfmodeType = (
        ("rfc2833", 'rfc2833'),
        ("info", 'info'),
        ("shortinfo", 'shortinfo'),
        ("inband", 'inband'),
        ("auto", 'auto'),
    )
    dtmfmode = models.CharField(max_length=10, choices=dtmfmodeType)
    directmedia = (
        ('yes', 'YES'),
        ('no', 'NO'),
        ('nonat', 'NONAT'),
        ('update', 'UPDATE'),
    )
    directmedia = models.CharField(max_length=10, choices=directmedia, blank=True, null=True, default='')
    nat = (
        ('yes', 'YES'),
        ('no', 'NO'),
        ('never', 'NEVER'),
        ('route', 'ROUTE'),
    )
    nat = models.CharField(max_length=10, choices=nat)

    # owner = models.ForeignKey('auth.User',  # ADD THIS FIELD
    #                           related_name='sip',
    #                           on_delete=models.CASCADE,
    #                           default='')

    def __str__(self):
        return "{}".format(self.name)


# class extensions(models.Model):
#     id = models.AutoField(primary_key=True)
#     context = models.CharField(max_length=20, unique=True)
#     exten = models.CharField(max_length=20)
#     priority = models.PositiveSmallIntegerField(default=0)
#     app = models.CharField(max_length=20)
#     appdata = models.CharField(max_length=128)
#
#     def __str__(self):
#         return "{}".format(self.context)
#
#
# class queue(models.Model):
#     id = models.AutoField(primary_key=True)
#     name = models.CharField(max_length=128, unique=True)
#     musiconhold = models.CharField(max_length=128, blank=True, null=True)
#     announce = models.CharField(max_length=128, blank=True, null=True)
#     context = models.CharField(max_length=128, blank=True, null=True)
#     timeout = models.PositiveSmallIntegerField(blank=True, null=True)
#     monitor_type = models.CharField(max_length=20, blank=True, null=True)
#     monitor_format = models.CharField(max_length=128, blank=True, null=True)
#     queue_youarenext = models.CharField(max_length=128, blank=True, null=True)
#     queue_thereare = models.CharField(max_length=128, blank=True, null=True)
#     queue_callswaiting = models.CharField(max_length=128, blank=True, null=True)
#     queue_holdtime = models.CharField(max_length=128, blank=True, null=True)
#     queue_minutes = models.CharField(max_length=128, blank=True, null=True)
#     queue_seconds = models.CharField(max_length=128, blank=True, null=True)
#     queue_lessthan = models.CharField(max_length=128, blank=True, null=True)
#     queue_thankyou = models.CharField(max_length=128, blank=True, null=True)
#     queue_reporthold = models.CharField(max_length=128, blank=True, null=True)
#     announce_frequency = models.PositiveSmallIntegerField(blank=True, null=True)
#     announce_round_seconds = models.PositiveSmallIntegerField(blank=True, null=True)
#     announce_holdtime = models.CharField(max_length=128, blank=True, null=True)
#     retry = models.PositiveSmallIntegerField(blank=True, null=True)
#     wrapuptime = models.PositiveSmallIntegerField(blank=True, null=True)
#     maxlen = models.PositiveSmallIntegerField(blank=True, null=True)
#     servicelevel = models.PositiveSmallIntegerField(blank=True, null=True)
#     strategy = models.CharField(max_length=20, blank=True, null=True)
#     joinempty = models.CharField(max_length=20, blank=True, null=True)
#     leavewhenempty = models.CharField(max_length=20, blank=True, null=True)
#     eventmemberstatus = models.NullBooleanField(blank=True)
#     eventwhencalled = models.NullBooleanField(blank=True)
#     reportholdtime = models.NullBooleanField(blank=True)
#     memberdelay = models.PositiveSmallIntegerField(blank=True, null=True)
#     weight = models.PositiveSmallIntegerField(blank=True, null=True)
#     timeoutrestart = models.NullBooleanField(blank=True)
#     periodic_announce = models.CharField(max_length=50, blank=True, null=True)
#     periodic_announce_frequency = models.PositiveSmallIntegerField(blank=True, null=True)
#     ringinuse = models.NullBooleanField(blank=True)
#     setinterfacevar = models.NullBooleanField(blank=True)
#
#     def __str__(self):
#         return "{}".format(self.name)
#
#
# class queue_member(models.Model):
#     uniqueid = models.AutoField(primary_key=True)
#     membername = models.CharField(max_length=128, blank=True, null=True)
#     queue_name = models.CharField(max_length=128)
#     interface = models.CharField(max_length=128, unique=True)
#     penalty = models.PositiveSmallIntegerField(blank=True, null=True)
#     paused = models.IntegerField(blank=True, null=True)
#
#     def __str__(self):
#         return "{}".format(self.queue_name)
#
#     class Meta:
#         ordering = ('uniqueid',)


# @receiver(post_save, sender=User)
# def create_auth_token(sender, instance=None, created=False, **kwargs):
#     if created:
#         Token.objects.create(user=instance)